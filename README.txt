***********
* README: *
***********

DESCRIPTION:
------------
This module provides an easy and configurable 
way to handle messages using the jnotify library


REQUIREMENTS:
-------------
The jnotify module requires the jnotify jquery 
library to be installed or updated
required modules libraries


INSTALLATION:
-------------
1. Place the entire email directory into your Drupal sites/all/modules/
   directory.

2. Enable the email module by navigating to:

     administer > modules


Features:
---------
  * configuration of message settings
  * configuration of message style
  * work with different messages 
  * configuration of different message positions


Author:
-------
Jason Acuna
me@tatewaky.com
