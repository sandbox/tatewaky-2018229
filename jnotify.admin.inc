<?php

/**
 * @file
 * Admin interface fucntions
 */

/**
 * Implementation of an admin form.
 */
function jnotify_admin_form() {

  $identifiers = variable_get('jnotify_identifier', 'jnotify-class');

  $form['jnotify_show'] = array(
    '#title' => t('Show'),
    '#type' => 'select',
    '#options' => array(
      'jNotify' => t('a Notice Box'),
      'jError' => t('an Error Box'),
      'jSuccess' => t('a Success Box'),
    ),
    '#default_value' => variable_get('jnotify_show', 0),
  );

  $form['jnotify_autohide'] = array(
    '#title' => t('Auto Hide?'),
    '#type' => 'select',
    '#options' => array('true' => t('Y'), 'false' => t('N')),
    '#default_value' => variable_get('jnotify_autohide', 'true'),
  );

  $form['jnotify_vertical'] = array(
    '#title' => t('Vertical Position'),
    '#type' => 'select',
    '#options' => array(
      'bottom' => t('bottom'),
      'top' => t('top'),
      'center' => t('center'),
    ),
    '#default_value' => variable_get('jnotify_vertical', 'bottom'),
  );

  $form['jnotify_horizontal'] = array(
    '#title' => t('Horizontal Position'),
    '#type' => 'select',
    '#options' => array(
      'left' => t('left'),
      'right' => t('right'),
      'center' => t('center'),
    ),
    '#default_value' => variable_get('jnotify_horizontal', 'center'),
  );

  $form['jnotify_during'] = array(
    '#title' => t('Show Jurify During'),
    '#type' => 'select',
    '#options' => array(
      '1000' => t('1 Seg'),
      '2000' => t('2 Segs'),
      '3000' => t('3 Segs'),
    ),
    '#default_value' => variable_get('jnotify_during', '1000'),
  );

  $form['jnotify_close'] = array(
    '#title' => t('Close notify in?'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => variable_get('jnotify_close', '1000'),
  );

  $form['jnotify_identifier'] = array(
    '#title' => t('Css Classes'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $identifiers,
    '#description' => t('Add all the css classes comma separated.'),
  );

  $form['anchor'] = array(
    '#type' => 'item',
    '#markup' => l(
        t('Click here to see the jNotify box'), '', array(
          'attributes' => array(
            'class' => str_replace(',', ' ', $identifiers)),
        )
    ),
  );

  $form['#validate'][] = 'jnotify_admin_validate';

  return system_settings_form($form);
}

/**
 * Implementation of a validate of the form.
 */
function jnotify_admin_validate($form, &$form_state) {
  $identifiers = isset($form_state['values']['jnotify_identifier']) ? $form_state['values']['jnotify_identifier'] : FALSE;

  if ($identifiers) {
    $identifiers = explode(',', $identifiers);
    $identifiers_list = array();
    foreach ($identifiers as $identifier) {

      $identifiers_list[] = filter_xss(trim($identifier));
    }
  }

  $form_state['values']['jnotify_identifier'] = implode(',', $identifiers_list);
}
